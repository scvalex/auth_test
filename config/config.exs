# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :auth_test,
  ecto_repos: [AuthTest.Repo]

# Configures the endpoint
config :auth_test, AuthTestWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Ygw0+LvTnP3kxXPODK2lV56h4xuTtvKC4zrGsk5/S80RLNY6KE+JuXXVj0lcNazu",
  render_errors: [view: AuthTestWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: AuthTest.PubSub,
  live_view: [signing_salt: "cidzZox0"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Pow authentication
config :auth_test, :pow,
  user: AuthTest.Users.User,
  repo: AuthTest.Repo,
  web_module: AuthTestWeb

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
