defmodule AuthTestWeb.PowControllerTest do
  @moduledoc """
  There's no explicit `pow` controller, but we still want to test this stuff.
  """
  use AuthTestWeb.ConnCase

  @valid_user %{
    email: "user@example.com",
    password: "12345678",
    password_confirmation: "12345678"
  }

  @valid_user_new %{
    email: "good@example.com",
    password: "goodpassword",
    password_confirmation: "goodpassword"
  }

  @invalid_user %{
    email: "bad@example.com",
    password: "1234"
  }

  setup do
    Repo.insert!(Users.User.changeset(%Users.User{}, @valid_user))
    :ok
  end

  test "GET /session/new", %{conn: conn} do
    conn = get(conn, "/session/new")
    assert html_response(conn, 200) =~ "Sign in"
  end

  test "Login with invalid user", %{conn: conn} do
    conn = post(conn, "/session", user: @invalid_user)
    assert html_response(conn, 200) =~ "Please verify your credentials"
    refute "auth_test_auth" in get_session(conn)
  end

  test "Login with valid user", %{conn: conn} do
    conn = post(conn, "/session", user: @valid_user)
    assert redirected_to(conn, 302) =~ "/"
    assert %{"auth_test_auth" => _} = get_session(conn)
  end

  test "GET /registration/new", %{conn: conn} do
    conn = get(conn, "/registration/new")
    assert html_response(conn, 200) =~ "Password confirmation"
  end

  test "Register with too short password", %{conn: conn} do
    conn =
      post(conn, "/registration",
        user: Map.put(@invalid_user, :password_confirmation, @invalid_user.password)
      )

    assert html_response(conn, 200) =~ "should be at least 8 character"
  end

  test "Register successfully", %{conn: conn} do
    conn = post(conn, "/registration", user: @valid_user_new)
    assert redirected_to(conn, 302) =~ "/"
    assert %{"auth_test_auth" => _} = get_session(conn)

    assert Repo.get_by(Users.User, email: @valid_user_new.email)
  end
end
