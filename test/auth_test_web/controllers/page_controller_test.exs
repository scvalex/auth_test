defmodule AuthTestWeb.PageControllerTest do
  use AuthTestWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Abstract Binary"
  end
end
