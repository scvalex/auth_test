defmodule AuthTestWeb.PageController do
  use AuthTestWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
