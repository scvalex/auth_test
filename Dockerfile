FROM alpine:3.12

RUN apk add --no-cache openssl ncurses-libs
RUN ln -s /bin/sh /usr/bin/sh

ARG APP_NAME

ENV REPLACE_OS_VARS=true
ENV APP_NAME=${APP_NAME}
ENV MIX_ENV=prod
ENV INSTALL_PATH=/app
ENV HOME=${INSTALL_PATH}

RUN mkdir -p ${INSTALL_PATH}
RUN chown nobody:nobody ${INSTALL_PATH}

WORKDIR ${INSTALL_PATH}

USER nobody:nobody

COPY --chown=nobody:nobody _build/${MIX_ENV}/rel/${APP_NAME}/ "${INSTALL_PATH}/"

ENTRYPOINT ${INSTALL_PATH}/bin/${APP_NAME} start
